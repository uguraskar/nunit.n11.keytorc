﻿using OpenQA.Selenium;

namespace NUnit.n11.Keytorc {
    class LoginPage {
        IWebDriver driver;

        By gotoLoginPage = By.XPath("/html/body/div[1]/header/div/div/div[2]/div[2]/div[2]/div/div/a[1]");
        By username = By.Id("email");
        By password = By.Id("password"); // sss
        By loginButton = By.Id("loginButton");

        public LoginPage(IWebDriver driver) {
            this.driver=driver;
            driver.FindElement(gotoLoginPage).Click();
            }

        public void typeUserName() {
            driver.FindElement(username).SendKeys("nixito@gifto12.com");
            }

        public void typePassword() {
            driver.FindElement(password).SendKeys("nixitogifto12");
            }

        public void clickOnLoginButton() {
            driver.FindElement(loginButton).Click();
            }
        }
    }
