﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace NUnit.n11.Keytorc {
    public class Favourites {
        IWebDriver driver;
        Actions action;

        By thirdProduct = By.XPath("/html/body/div[1]/div/div/div/div[2]/section/div[3]/ul/li[3]/div/div[1]/a/h3");
        By favProduct = By.XPath("/html/body/div[1]/div/div/div[2]/div[3]/div/div[2]/ul/li/div/div[1]/a/h3");
        By addThirdToFavsButton = By.XPath("/html/body/div[1]/div/div/div/div[2]/section/div[3]/ul/li[3]/div/div[2]/span[2]");
        By MyAccountHolder = By.XPath("/html/body/div[1]/header/div/div/div[2]/div[2]/div[2]");
        By gotoFavsButton = By.XPath("/html/body/div[1]/header/div/div/div[2]/div[2]/div[2]/div[2]/div/a[2]");
        By selectFirstFavButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[3]/ul/li[1]/div/ul/li/a/img");
        By deleteFromFavsButton = By.XPath("/html/body/div[1]/div/div/div[2]/div[3]/div/div[2]/ul/li/div/div[3]/span");
        By confirmationMessageBox = By.XPath("/html/body/div[4]/div/i");

        string thirdProductValue;
        public Favourites(IWebDriver driver, Actions action) {
            this.driver=driver;
            this.action=action;
            }

        public void addThirdToFavs() {
            IWebElement we = driver.FindElement(thirdProduct);
            thirdProductValue=we.Text;
            driver.FindElement(addThirdToFavsButton).Click();
            }

        public void hoverOnMyAccountHolder() {
            IWebElement we = driver.FindElement(MyAccountHolder);
            action.MoveToElement(we).Perform();
            }

        public void gotoFavs() {
            driver.FindElement(gotoFavsButton).Click();
            }

        public void selectFirstFav() {
            driver.FindElement(selectFirstFavButton).Click();
            }

        public bool isProductSame() {
            bool same = false;
            IWebElement we = driver.FindElement(favProduct);
            if(thirdProductValue==we.Text) {
                same=true;
                } else {
                same=false;
                }
            Console.WriteLine("Is favourite product same as searched product? "+same);
            return same;
            }

        public void deleteFromFavs() {
            driver.FindElement(deleteFromFavsButton).Click();
            }

        public bool isProductDeletedFromFavs() {
            bool deleted = true;
            try {
                driver.FindElement(confirmationMessageBox);
                } catch(NoSuchElementException) {
                deleted=false;
                }
            Console.WriteLine("Is product deleted from favourites? "+deleted);
            return deleted;
            }
        }
    }
