﻿using System;
using OpenQA.Selenium;

namespace NUnit.n11.Keytorc {
    class SearchItem {
        IWebDriver driver;

        By searchTextBox = By.Id("searchData");
        By searchButton = By.XPath("/html/body/div[1]/header/div/div/div[2]/div[1]/a");
        By notFoundContainer = By.XPath("/html/body/div[1]/div/div/div/div/div/div/div[2]");
        By secondPage = By.XPath("/html/body/div[1]/div/div/div/div[2]/div[3]/a[2]");
        string searchText = "samsung";
        public SearchItem(IWebDriver driver) {
            this.driver=driver;
            }

        public void typeSearchText() {
            driver.FindElement(searchTextBox).SendKeys(searchText);
            }

        public void clickOnSearchButton() {
            driver.FindElement(searchButton).Click();
            }

        public bool isExist() {
            bool exist = false;
            try {
                driver.FindElement(notFoundContainer);
                } catch(NoSuchElementException) {
                exist=true;
                }
            Console.WriteLine("Search Found: "+exist);
            return exist;
            }

        public void gotoSecondPage() {
            driver.FindElement(secondPage).Click();
            }

        public bool confirmSecondPage() {
            bool confirm = false;
            string Title = driver.Title;
            int index = Title.IndexOf("/");
            if(index>0) {
                Title=Title.Substring(0, index);
                }
            Title=Title.ToLower().Replace(searchText+" - n11.com - ", "");
            if(Title=="2") {
                confirm=true;
                } else {
                confirm=false;
                }
            Console.WriteLine("In Second Page? "+confirm);
            return confirm;
            }
        }
    }
