﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;

namespace NUnit.n11.Keytorc {
    [TestFixture]
    public class TestClass {
        [Test]
        public void testWholeProgram() {
            IWebDriver driver = new FirefoxDriver();
            driver.Url="https://www.n11.com/";

            Actions action = new Actions(driver);

            LoginPage login = new LoginPage(driver);
            login.typeUserName();
            login.typePassword();
            login.clickOnLoginButton();

            SearchItem search = new SearchItem(driver);
            search.typeSearchText();
            search.clickOnSearchButton();
            search.isExist();
            search.gotoSecondPage();
            search.confirmSecondPage();

            Favourites favs = new Favourites(driver, action);
            favs.addThirdToFavs();
            favs.hoverOnMyAccountHolder();
            favs.gotoFavs();
            favs.selectFirstFav();
            favs.isProductSame();
            favs.deleteFromFavs();
            favs.isProductDeletedFromFavs();
            }

        [Test]
        public void testLogin() {
            IWebDriver driver = new FirefoxDriver();
            driver.Url="https://www.n11.com/";

            Actions action = new Actions(driver);

            LoginPage login = new LoginPage(driver);
            login.typeUserName();
            login.typePassword();
            login.clickOnLoginButton();
            }

        [Test]
        public void testSearch() {
            IWebDriver driver = new FirefoxDriver();
            driver.Url="https://www.n11.com/";

            Actions action = new Actions(driver);

            LoginPage login = new LoginPage(driver);
            login.typeUserName();
            login.typePassword();
            login.clickOnLoginButton();

            SearchItem search = new SearchItem(driver);
            search.typeSearchText();
            search.clickOnSearchButton();
            search.isExist();
            search.gotoSecondPage();
            search.confirmSecondPage();
            }

        [Test]
        public void testFavs() {
            IWebDriver driver = new FirefoxDriver();
            driver.Url="https://www.n11.com/";

            Actions action = new Actions(driver);

            LoginPage login = new LoginPage(driver);
            login.typeUserName();
            login.typePassword();
            login.clickOnLoginButton();

            SearchItem search = new SearchItem(driver);
            search.typeSearchText();
            search.clickOnSearchButton();
            search.isExist();
            search.gotoSecondPage();
            search.confirmSecondPage();

            Favourites favs = new Favourites(driver, action);
            favs.addThirdToFavs();
            favs.hoverOnMyAccountHolder();
            favs.gotoFavs();
            favs.selectFirstFav();
            favs.isProductSame();
            favs.deleteFromFavs();
            favs.isProductDeletedFromFavs();
            }
        }
    }
